import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { AlertController, NavController } from 'ionic-angular';
import { CartData } from '../../providers/cart-data';
import { CheckoutPage } from '../../pages/checkout/checkout';
import { FormGroup, FormBuilder , Validators} from '@angular/forms';

/**
 * Generated class for the PantalonhombrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pantalonhombre',
  templateUrl: 'pantalonhombre.html',
})
export class PantalonhombrePage {
  public totalPrice: any;
  public data: any;	
  public orders: any;
  public total_ordered_items: any;
  cantidad:string='';
  myForm: FormGroup;

  constructor(public fb: FormBuilder, public navCtrl: NavController, private http: Http, public alertCtrl: AlertController, public cart_data: CartData) {
    this.data;

    this.cart_data.loadAll().then(result => {
      this.orders = result;
    });

    this.cart_data.getSize().then(result => {
      this.total_ordered_items = result;
    });

  this.myForm = this.fb.group({
      cantidad: ['', [Validators.required,
        // Validators.minLength(1),
          //Validators.maxLength(2),
          Validators.max(20),
          Validators.min(1),
        ],
],
    });
    this.cantidad = this.myForm.value.cantidad 

  }
   
  ionViewDidLoad() {
    console.log("entro aqui primero");
 this.getData();
   }

  getData(){
		this.http.get('assets/data/pantaloneshombre.json').map(res => res.json()).subscribe(
		data => {
			this.data = data;
      console.log(this.data);
		}, (rej) => {console.error("No hay respuesta",rej)});
  }

  showConfirm(item,price,cantidad,total) {
    console.log(cantidad);
    this.totalPrice = price * this.myForm.value.cantidad ;
    console.log(this.totalPrice);
    let confirm = this.alertCtrl.create({
      title: 'Añadir al carrito?',
      message: 'Por favor confirme si quiere agregar el producto carrito',
      buttons: [
        {
          text: 'Si',
          handler: () => {
            this.cart_data.addItem(item, price,this.myForm.value.cantidad, this.totalPrice);
          }
        },
        {
          text: 'Cancelar',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  toCar(){
    this.navCtrl.push(CheckoutPage);
  }

}
