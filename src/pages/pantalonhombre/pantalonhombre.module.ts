import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PantalonhombrePage } from './pantalonhombre';

@NgModule({
  declarations: [
    PantalonhombrePage,
  ],
  imports: [
    IonicPageModule.forChild(PantalonhombrePage),
  ],
})
export class PantalonhombrePageModule {}
