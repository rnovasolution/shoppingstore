import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ActionSheetController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { CartData } from '../../providers/cart-data';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  myForm: FormGroup;

  constructor( public store: Storage,public alertCtrl: AlertController, private http: Http, public cart_data: CartData, public navCtrl: NavController, public navParams: NavParams,     public fb: FormBuilder
  	) {
this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.pattern(/^[a-z0-9_-]{6,18}$/)]],
    });
}
  ionViewDidLoad() {}
saveData(){
  localStorage.setItem('username', this.myForm.value.username);
  localStorage.setItem('name',this.myForm.value.name);
  localStorage.setItem('email', this.myForm.value.email);
   this.cart_data.registerUser(this.myForm.value.name, this.myForm.value.username, this.myForm.value.email, this.myForm.value.password).then((res)=>{              
              this.showMessage();
            },(err)=>{
              this.showErrorMessage();
            });
  }

 showMessage() {
      let alert = this.alertCtrl.create({
        title: '<br><center>Succes</center></br>',
        subTitle: '<br><center>Usuario Registrado</center></br>.',
        buttons: ['OK']
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
}
showErrorMessage() {
      let alert = this.alertCtrl.create({
       title: '<br><center>Ops!</center></br>',
       subTitle: '<br><center>No se pudo realizar el pedido intente nuevamente.</br></center>',
       buttons: ['OK']
      });
      alert.present();
  }
 
}
