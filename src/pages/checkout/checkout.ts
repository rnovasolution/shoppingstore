import { Component } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CartData } from '../../providers/cart-data';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  myForm: FormGroup;
  public orders: any;    
  public prices: any; 
  public totals: any;   
  public totals1: any;   
  public totals2: any;   
  public data;     
  public items = '';   
  public loadingPopup: any;   
  totalBill: {};   
  public total = 0;
  username:string="";
  phone:number;
  direccion:string="";
  email:string="";
  ci:number;
  month:Date;
  ubicacion:string="";
  name;
  //myDate2: String = new Date().toISOString().split('T')[0];
  constructor(public fb: FormBuilder, platform: Platform,public navCtrl: NavController, private http: Http, public cart_data: CartData, public alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  	this.myForm = this.fb.group({
      username: ['', [Validators.required]],
      phone: ['', [Validators.minLength(8), Validators.maxLength(8)]],
      entrega: [''],
      month: [''],
      ci: [''],
      direccion: [''],
      email: ['', [Validators.required, Validators.email]],
    });
    this.cart_data.loadAllItemsOrdered().then(result => {
      this.orders = result;
    });

    this.cart_data.getTotalBill().then(result => {
      this.totalBill = result;
      console.log(this.totalBill);
    });

    this.data = {};
    this.data.trans_info = '';
	  this.data.username = '';
	  this.data.phone = '';
	  this.data.payment = '';
	  this.data.response = '';
	  this.http = http;
    this.name = localStorage.getItem('name');
  }
  
  sendSaleCart(){

    console.log(JSON.stringify(this.myForm.value));
    console.log(this.myForm.value.email);
     this.cart_data.sendSale(this.myForm.value.username, this.myForm.value.phone, this.myForm.value.direccion, this.myForm.value.email, this.myForm.value.ci, this.myForm.value.month).then((res)=>{              
              this.showMessage();
            },(err)=>{
              this.showErrorMessage();
            });
  }
 showMessage() {
      let alert = this.alertCtrl.create({
        title: '<br><center>Pedido Exitoso</center></br>',
        subTitle: '<br><center>Su pedido se realizo exitosamente</center></br>.',
        buttons: ['OK']
    });
    alert.present();
    this.confirm();
    //this.navCtrl.push(HomePage);
  //this.navCtrl.pop();
  }  

  showErrorMessage() {
      let alert = this.alertCtrl.create({
       title: '<br><center>Ops!</center></br>',
       subTitle: '<br><center>No se pudo realizar el pedido intente nuevamente.</br></center>',
       buttons: ['OK']
      });
      alert.present();
  }
  confirm(){
        window.location.reload();

  }


    		/*let headers = new Headers();
	    headers.append('Content-Type', 'application/json' );

	    for(let order of this.orders) {
			   this.items += order.title+",";
		  }

	    let postParams = { trans_info: this.data.username + "-" + this.data.phone , trans_info2: this.items }

		var link = 'http://localhost:8100/mystore/api.php';

		this.http.post(link, JSON.stringify(postParams), {headers: headers})
		.subscribe(data => {
			this.data.response = data["_body"];
			alert("Your orders has been received and will be processed shortly.");
		}, error => {
			console.log("Error while processing your orders.");
		});*/

  showConfirm(ndx) {
    console.log(ndx);
    let confirm = this.alertCtrl.create({
      title: 'Quitar este item del Carrito?',
      cssClass: 'alertcss',
      message: 'Confirmar Por Favor.',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.cart_data.removeItem(ndx);

            this.cart_data.getTotalBill().then(result => {
              this.totalBill = result;
              console.log(this.totalBill);
            });
    
          }
        },
        {
          text: 'Cancel',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  sendConfirm(ndx) {
    let confirm = this.alertCtrl.create({
      title: 'Proceder con el pedido?',
      message: 'Por favor confirmar.',
      buttons: [
        {
          text: 'Si',
          handler: ()  => {
            window.location.reload();
            //this.navCtrl.setRoot(this.navCtrl.getActive().component);
          }          
        },
        {
          text: 'Cancelar',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }



  

}
