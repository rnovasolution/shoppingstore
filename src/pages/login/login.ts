import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { RegisterPage } from '../../pages/register/register';
import { CartData } from '../../providers/cart-data';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  myForm: FormGroup;
  nameUser;
  constructor( public store: Storage, public alertCtrl: AlertController, private http: Http, public cart_data: CartData, public fb: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
  	this.myForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });

    //this.store.set('username', JSON.stringify(this.myForm.value.username));
    //this.store.set('password', JSON.stringify(this.myForm.value.password));
  }

  login(){
    this.nameUser = this.myForm.value.username;
    localStorage.setItem('username', this.myForm.value.username);
    console.log('this.myForm.value.username');
    console.log(this.store.set('username', JSON.stringify(this.myForm.value.username)));

    this.cart_data.loginUser(this.myForm.value.username, this.myForm.value.password).then((res)=>{              
              this.showMessage();
            },(err)=>{
              this.showErrorMessage();
            });
  }
  showMessage() {
      let alert = this.alertCtrl.create({
        title: '<br><center>Succes</center></br>',
        subTitle: "<br><center>Bienvenido </br>" + this.nameUser + "<br> a Damagis Jeans</center></br>.",
        buttons: ['OK']
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
}
showErrorMessage() {
      let alert = this.alertCtrl.create({
       title: '<br><center>Ops!</center></br>',
       subTitle: '<br><center>El usuario no existe, inténtelo otra vez</br></center>',
       buttons: ['OK']
      });
      alert.present();
  }

  signUp(){
        this.navCtrl.push(RegisterPage);

  }

}
