import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../../pages/login/login';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
name;
username;
email;
  constructor(private store: Storage, public nav: NavController, public navParams: NavParams) {
  this.username = localStorage.getItem('username');

this.name = localStorage.getItem('name');

this.email = localStorage.getItem('email');
}


  ionViewDidLoad() {
console.log(this.username);
}
deleteCache(){
localStorage.removeItem('username');
this.nav.setRoot(LoginPage);
}
}
