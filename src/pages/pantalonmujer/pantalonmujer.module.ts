import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PantalonmujerPage } from './pantalonmujer';

@NgModule({
  declarations: [
    PantalonmujerPage,
  ],
  imports: [
    IonicPageModule.forChild(PantalonmujerPage),
  ],
})
export class PantalonmujerPageModule {}
