import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PantalonhombrePage } from '../pantalonhombre/pantalonhombre';
import { PantalonmujerPage } from '../pantalonmujer/pantalonmujer';
import { Storage } from '@ionic/storage';

import { CartData } from '../../providers/cart-data';
import { CheckoutPage } from '../../pages/checkout/checkout';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public total_ordered_items: any;

  constructor( private store: Storage,public navCtrl: NavController, public cart_data: CartData) {
    this.cart_data.getSize().then(result => {
      this.total_ordered_items = result;
    });

  }

  ionViewDidLoad() {

  }
  gotoPastaPage() {
  	this.navCtrl.push(PantalonhombrePage);
  }

  gotoPizzaPage() {
  	this.navCtrl.push(PantalonmujerPage);
  }

  gotoStartersPage() {
  	
  }

  toCar(){
    this.navCtrl.push(CheckoutPage);
  }

}
