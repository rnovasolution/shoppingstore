import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformativePage } from './informative';

@NgModule({
  declarations: [
    InformativePage,
  ],
  imports: [
    IonicPageModule.forChild(InformativePage),
  ],
})
export class InformativePageModule {}
