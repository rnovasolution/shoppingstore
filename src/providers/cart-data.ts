import { Injectable } from '@angular/core';
import {  Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CartData {
  items: any = []; 
  public total = 0;
  data;
  serverBaseUrl="http://c1210145.ferozo.com/backend/web/index.php?r=api/";
  constructor(public http: Http) {}


  sendSale(nombre, telefono, direccion, email, ci, fechapedido){
    var url = this.serverBaseUrl+"pedido/postpedido/";
    var headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
    var ops = new RequestOptions({headers:headers});
    var formdata = "'&nombre="+nombre+"&telefono="+telefono+"&direccion="+direccion+"&email="+email+"&ci="+ci+"+&fechapedido="+fechapedido+"";
    return new Promise((resolve, reject)=>{
      this.http.post(encodeURI(url),formdata,ops).map((res:Response) => res.json())
      .subscribe(data=>{
        this.data = data;
        console.log(this.data);
        resolve(this.data);
      },err=>reject(err));
    }) 
  }
  registerUser(name, username, email, password){
    var url = this.serverBaseUrl+"cliente/postcliente/";
    var headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
    var ops = new RequestOptions({headers:headers});
    var formdata = "'&name="+name+"&username="+username+"&email="+email+"&password="+password+"";
    return new Promise((resolve, reject)=>{
      this.http.post(encodeURI(url),formdata,ops).map((res:Response) => res.json())
      .subscribe(data=>{
        this.data = data;
        console.log(this.data);
        resolve(this.data);
      },err=>reject(err));
    }) 
  }
  loginUser(username,  password){
    var url = this.serverBaseUrl+"cliente/authenticate";
    var headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
    var ops = new RequestOptions({headers:headers});
    var formdata = "'&username="+username+"&password="+password+"";
    return new Promise((resolve, reject)=>{
      this.http.post(encodeURI(url),formdata,ops).map((res:Response) => res.json())
      .subscribe(data=>{
        this.data = data;
        console.log(this.data);
        resolve(this.data);
      },err=>reject(err));
    }) 
  }

  addItem(item, price, cantidad, total){
        this.items.push({title: item, amount: price, cantidad: cantidad, total: total});
  }

  removeItem(ndx) {
      this.items.splice(ndx, 1);
  }

  loadAll(){
    return Promise.resolve(this.items);
  };

  loadAllItemsOrdered(){
    return Promise.resolve(this.items);
  }

  getTotalBill() {
      this.total = 0;
      for(let item of this.items) {
          this.total += parseInt(item.total); 
          console.log("this.total");         
          console.log(this.total);         
      }
      return Promise.resolve(this.total);
  }

  getSize() {
    return Promise.resolve(this.items);
  }

}
