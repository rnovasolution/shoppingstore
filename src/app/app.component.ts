import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { PantalonhombrePage } from '../pages/pantalonhombre/pantalonhombre';
import { PantalonmujerPage } from '../pages/pantalonmujer/pantalonmujer';
import { CheckoutPage } from '../pages/checkout/checkout';
import { InformativePage } from '../pages/informative/informative';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { Storage } from '@ionic/storage';
import { ProfilePage } from '../pages/profile/profile';
import { PromotionsPage } from '../pages/promotions/promotions';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
showSubmenu: boolean = false;

  rootPage: any = HomePage;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor(private store: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', icon:"ios-home", component: HomePage },
      { title: 'Perfil', icon:"ios-contact", component: ProfilePage },
      { title: 'Promociones', icon:"ios-basket",component: PromotionsPage },
      { title: 'Carrito de Compra',icon:"ios-cart", component: CheckoutPage },
      { title: 'Información', icon:"ios-information-circle",component: InformativePage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      localStorage.getItem('username');
      console.log(localStorage.getItem('username'));
         if(localStorage.getItem('username') == null )
      {
        this.nav.setRoot(LoginPage);
      }
      else
      {
        this.nav.setRoot(HomePage);
      }



    /*this.store.get('username').then((val) => {
      });*/
  
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
   
}
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  menuItemHandler(): void {
  this.showSubmenu = !this.showSubmenu;
}

openMen(){
    this.nav.setRoot(PantalonhombrePage);
}
openWomen(){
    this.nav.setRoot(PantalonmujerPage);
}
}
