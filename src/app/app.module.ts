import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PantalonhombrePage } from '../pages/pantalonhombre/pantalonhombre';
import { PantalonmujerPage } from '../pages/pantalonmujer/pantalonmujer';
import { CheckoutPage } from '../pages/checkout/checkout';
import { InformativePage } from '../pages/informative/informative';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ProfilePage } from '../pages/profile/profile';
import { PromotionsPage } from '../pages/promotions/promotions';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CartData } from '../providers/cart-data';
import { IonicStorageModule } from '@ionic/storage';
import { Storage } from '@ionic/storage';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PantalonhombrePage,
    PantalonmujerPage,
    CheckoutPage,
    InformativePage,
    LoginPage,
    RegisterPage,
    ProfilePage,
    PromotionsPage
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
     PantalonhombrePage,
    PantalonmujerPage,
    CheckoutPage,
    InformativePage,
    LoginPage,
    RegisterPage,
    ProfilePage,
    PromotionsPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CartData
  ]
})
export class AppModule {}
