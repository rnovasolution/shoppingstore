webpackJsonp([5],{

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformativePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the InformativePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InformativePage = /** @class */ (function () {
    function InformativePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InformativePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InformativePage');
    };
    InformativePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-informative',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\informative\informative.html"*/'\n<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Información</ion-title>\n  \n  \n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\informative\informative.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], InformativePage);
    return InformativePage;
}());

//# sourceMappingURL=informative.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(store, alertCtrl, http, cart_data, navCtrl, navParams, fb) {
        this.store = store;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.cart_data = cart_data;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.myForm = this.fb.group({
            name: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            username: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(10)]],
            email: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].email]],
            password: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].pattern(/^[a-z0-9_-]{6,18}$/)]],
        });
    }
    RegisterPage.prototype.ionViewDidLoad = function () { };
    RegisterPage.prototype.saveData = function () {
        var _this = this;
        localStorage.setItem('username', this.myForm.value.username);
        localStorage.setItem('name', this.myForm.value.name);
        localStorage.setItem('email', this.myForm.value.email);
        this.cart_data.registerUser(this.myForm.value.name, this.myForm.value.username, this.myForm.value.email, this.myForm.value.password).then(function (res) {
            _this.showMessage();
        }, function (err) {
            _this.showErrorMessage();
        });
    };
    RegisterPage.prototype.showMessage = function () {
        var alert = this.alertCtrl.create({
            title: '<br><center>Succes</center></br>',
            subTitle: '<br><center>Usuario Registrado</center></br>.',
            buttons: ['OK']
        });
        alert.present();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    RegisterPage.prototype.showErrorMessage = function () {
        var alert = this.alertCtrl.create({
            title: '<br><center>Ops!</center></br>',
            subTitle: '<br><center>No se pudo realizar el pedido intente nuevamente.</br></center>',
            buttons: ['OK']
        });
        alert.present();
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\register\register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Formulario de registro</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<form [formGroup]="myForm" (ngSubmit)="saveData()" novalidate>\n  <ion-list> \n    <ion-item>\n      <ion-label stacked>Nombre:</ion-label>\n      <ion-input formControlName="name" type="text" placeholder="name"></ion-input>\n    </ion-item>\n    <ion-item *ngIf="myForm.get(\'name\').errors && myForm.get(\'name\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'name\').hasError(\'required\')">Field is required</p>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked>Username:</ion-label>\n      <ion-input formControlName="username" type="text" placeholder="username"></ion-input>\n    </ion-item>\n    <ion-item *ngIf="myForm.get(\'username\').errors && myForm.get(\'username\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'username\').hasError(\'required\')">Field is required</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'username\').hasError(\'minlength\')">Min of 5 characters</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'username\').hasError(\'maxlength\')">Max of 10 characters</p>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked>E-mail:</ion-label>\n      <ion-input formControlName="email" type="text" placeholder="email"></ion-input>\n    </ion-item>\n    <ion-item *ngIf="myForm.get(\'email\').errors && myForm.get(\'email\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'email\').hasError(\'required\')">Field is required</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'email\').hasError(\'email\')">It is not an email</p>\n    </ion-item>\n\n    \n    \n    <ion-item>\n      <ion-label stacked>Password:</ion-label>\n      <ion-input formControlName="password" type="password" placeholder="password"></ion-input>\n    </ion-item>\n    <ion-item *ngIf="myForm.get(\'password\').errors && myForm.get(\'password\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'password\').hasError(\'required\')">Field is required</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'password\').hasError(\'pattern\')">It is not a strong password</p>\n    </ion-item>\n  </ion-list>\n  <div padding>\n    <button ion-button block type="submit" [disabled]="myForm.invalid">Guardar</button>\n  </div>\n</form>\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = /** @class */ (function () {
    function ProfilePage(store, nav, navParams) {
        this.store = store;
        this.nav = nav;
        this.navParams = navParams;
        this.username = localStorage.getItem('username');
        this.name = localStorage.getItem('name');
        this.email = localStorage.getItem('email');
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log(this.username);
    };
    ProfilePage.prototype.deleteCache = function () {
        localStorage.removeItem('username');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\profile\profile.html"*/'<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title align="center">Perfil</ion-title>\n\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n<ion-list>\n  <ion-item>\n  	<div align="center">\n    <ion-avatar item-start>\n      <img src="img/administrator.png">\n    </ion-avatar>\n  		\n  	</div>\n   \n  </ion-item>\n  <ion-item align="center">\n  	<div align="center">\n  		<p>{{username}}</p>\n  	</div>\n  </ion-item>\n    <ion-item align="center">\n  	<div align="center">\n  		<p>{{name}}</p>\n  	</div>\n  </ion-item>\n\n    <ion-item align="center">\n  	<div align="center">\n  		<p>{{email}}</p>\n  	</div>\n  </ion-item>\n</ion-list>\n<div align="center">\n	<button ion-button full (click)="deleteCache()">Logout</button>\n</div>\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 119:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 119;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/home/home.module": [
		284,
		4
	],
	"../pages/informative/informative.module": [
		285,
		3
	],
	"../pages/login/login.module": [
		286,
		2
	],
	"../pages/profile/profile.module": [
		287,
		1
	],
	"../pages/register/register.module": [
		288,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PromotionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_checkout_checkout__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PromotionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PromotionsPage = /** @class */ (function () {
    function PromotionsPage(fb, navCtrl, http, alertCtrl, cart_data) {
        var _this = this;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.cart_data = cart_data;
        this.cantidad = '';
        this.data;
        this.getData();
        this.cart_data.loadAll().then(function (result) {
            _this.orders = result;
            console.log(_this.orders);
        });
        this.cart_data.getSize().then(function (result) {
            _this.total_ordered_items = result;
            console.log(_this.total_ordered_items);
        });
        this.myForm = this.fb.group({
            cantidad: ['', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(1),
                    __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(2)]
            ],
        });
        this.cantidad = this.myForm.value.cantidad;
    }
    PromotionsPage.prototype.getData = function () {
        var _this = this;
        this.http.get('assets/data/promociones.json').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.data = data;
            console.log(_this.data);
        }, function (rej) { console.error("Could not load local data", rej); });
    };
    PromotionsPage.prototype.showConfirm = function (item, price, cantidad, total) {
        var _this = this;
        console.log(cantidad);
        this.totalPrice = price * this.myForm.value.cantidad;
        console.log(this.totalPrice);
        var confirm = this.alertCtrl.create({
            title: 'Añadir al carrito?',
            message: 'Por favor confirme si quiere agregar el producto carrito',
            buttons: [
                {
                    text: 'Si',
                    handler: function () {
                        _this.cart_data.addItem(item, price, _this.myForm.value.cantidad, _this.totalPrice);
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    PromotionsPage.prototype.toCar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_checkout_checkout__["a" /* CheckoutPage */]);
    };
    PromotionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-promotions',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\promotions\promotions.html"*/'<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Promociones</ion-title>\n    <div *ngIf="total_ordered_items?.length > 0" style="position:absolute;right:32px;top:18px;background-color:#488AFF;padding:4px 8px;border-radius:50%;color:#fff;font-size:10px;">\n        {{total_ordered_items.length}}\n    </div>\n    <ion-buttons end>\n        <button  (click)="toCar()" style="background-color:transparent;"><ion-icon id="car" name="cart"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n<form [formGroup]="myForm" novalidate>\n\n<ion-card *ngFor="let dat of data" style="background-color:#f6f6f6;margin-bottom:25px;">\n  <img src="img/{{ dat.img }}.jpeg" />\n  <ion-card-content>\n    <ion-card-title>\n      {{ dat.title }}\n    </ion-card-title>\n    <div><h4>Color</h4>\n<ion-item>\n  <ion-select>\n <ion-option [value]="item.value" *ngFor="let item of dat.color;let i = index;" style="margin-bottom:30px;">{{item.value}}</ion-option>\n      </ion-select>  \n</ion-item></div>\n\n   <div><h4>Talla</h4>\n<ion-item>\n  <ion-select >\n <ion-option [value]="item.name" *ngFor="let item of dat.tallas;let i = index;" style="margin-bottom:30px;">{{item.value}}</ion-option>  </ion-select>\n</ion-item></div>\n<div>\n  <h4>Cantidad</h4>\n <ion-item  >\n      \n    <ion-input  formControlName="cantidad" placeholder="Ingrese cantidad" type="tel"    ></ion-input>\n    <!--<p color="danger" ion-text *ngIf="maxlength="20" && minLength="1".hasError(\'cantidad\')">It is not an cantidad 30</p>-->\n\n  </ion-item>\n  <ion-item *ngIf="myForm.get(\'cantidad\').errors && myForm.get(\'cantidad\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'cantidad\').hasError(\'required\')">Campo Requedido</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'cantidad\').hasError(\'maxlength\')"> Cantidad max 99</p>\n    </ion-item>\n</div>\n    <p *ngFor="let item of dat.items;let i = index;" style="margin-bottom:30px;">\n      {{ item.details }}<br><br>\n      \n      <span style="font-size:15px;font-weight:bold;float:right;">Bs{{ item.price }}</span>\n      <button *ngIf="i==0" ion-button block (click)="showConfirm(dat.title,item.price,cantidad);" [disabled]="myForm.invalid"><ion-icon name="cart"></ion-icon>Agregar a Carrito</button>\n    </p>\n\n  </ion-card-content>\n</ion-card>\n</form>\n\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\promotions\promotions.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */]) === "function" && _e || Object])
    ], PromotionsPage);
    return PromotionsPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=promotions.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(229);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pantalonhombre_pantalonhombre__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_pantalonmujer_pantalonmujer__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_checkout_checkout__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_informative_informative__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_register_register__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_promotions_promotions__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_storage__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_pantalonhombre_pantalonhombre__["a" /* PantalonhombrePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_pantalonmujer_pantalonmujer__["a" /* PantalonmujerPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_informative_informative__["a" /* InformativePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_promotions_promotions__["a" /* PromotionsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/informative/informative.module#InformativePageModule', name: 'InformativePage', segment: 'informative', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_17__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_pantalonhombre_pantalonhombre__["a" /* PantalonhombrePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_pantalonmujer_pantalonmujer__["a" /* PantalonmujerPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_informative_informative__["a" /* InformativePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_promotions_promotions__["a" /* PromotionsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_16__providers_cart_data__["a" /* CartData */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartData; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CartData = /** @class */ (function () {
    function CartData(http) {
        this.http = http;
        this.items = [];
        this.total = 0;
        this.serverBaseUrl = "http://c1210145.ferozo.com/backend/web/index.php?r=api/";
    }
    CartData.prototype.sendSale = function (nombre, telefono, direccion, email, ci, fechapedido) {
        var _this = this;
        var url = this.serverBaseUrl + "pedido/postpedido/";
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var ops = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var formdata = "'&nombre=" + nombre + "&telefono=" + telefono + "&direccion=" + direccion + "&email=" + email + "&ci=" + ci + "+&fechapedido=" + fechapedido + "";
        return new Promise(function (resolve, reject) {
            _this.http.post(encodeURI(url), formdata, ops).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.data = data;
                console.log(_this.data);
                resolve(_this.data);
            }, function (err) { return reject(err); });
        });
    };
    CartData.prototype.registerUser = function (name, username, email, password) {
        var _this = this;
        var url = this.serverBaseUrl + "cliente/postcliente/";
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var ops = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var formdata = "'&name=" + name + "&username=" + username + "&email=" + email + "&password=" + password + "";
        return new Promise(function (resolve, reject) {
            _this.http.post(encodeURI(url), formdata, ops).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.data = data;
                console.log(_this.data);
                resolve(_this.data);
            }, function (err) { return reject(err); });
        });
    };
    CartData.prototype.loginUser = function (username, password) {
        var _this = this;
        var url = this.serverBaseUrl + "cliente/authenticate";
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var ops = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var formdata = "'&username=" + username + "&password=" + password + "";
        return new Promise(function (resolve, reject) {
            _this.http.post(encodeURI(url), formdata, ops).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.data = data;
                console.log(_this.data);
                resolve(_this.data);
            }, function (err) { return reject(err); });
        });
    };
    CartData.prototype.addItem = function (item, price, cantidad, total) {
        this.items.push({ title: item, amount: price, cantidad: cantidad, total: total });
    };
    CartData.prototype.removeItem = function (ndx) {
        this.items.splice(ndx, 1);
    };
    CartData.prototype.loadAll = function () {
        return Promise.resolve(this.items);
    };
    ;
    CartData.prototype.loadAllItemsOrdered = function () {
        return Promise.resolve(this.items);
    };
    CartData.prototype.getTotalBill = function () {
        this.total = 0;
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            this.total += parseInt(item.total);
            console.log("this.total");
            console.log(this.total);
        }
        return Promise.resolve(this.total);
    };
    CartData.prototype.getSize = function () {
        return Promise.resolve(this.items);
    };
    CartData = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], CartData);
    return CartData;
}());

//# sourceMappingURL=cart-data.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_pantalonhombre_pantalonhombre__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pantalonmujer_pantalonmujer__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_checkout_checkout__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_informative_informative__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_promotions_promotions__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = /** @class */ (function () {
    function MyApp(store, platform, statusBar, splashScreen) {
        this.store = store;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.showSubmenu = false;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Inicio', icon: "ios-home", component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'Perfil', icon: "ios-contact", component: __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__["a" /* ProfilePage */] },
            { title: 'Promociones', icon: "ios-basket", component: __WEBPACK_IMPORTED_MODULE_12__pages_promotions_promotions__["a" /* PromotionsPage */] },
            { title: 'Carrito de Compra', icon: "ios-cart", component: __WEBPACK_IMPORTED_MODULE_7__pages_checkout_checkout__["a" /* CheckoutPage */] },
            { title: 'Información', icon: "ios-information-circle", component: __WEBPACK_IMPORTED_MODULE_8__pages_informative_informative__["a" /* InformativePage */] },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            localStorage.getItem('username');
            console.log(localStorage.getItem('username'));
            if (localStorage.getItem('username') == null) {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]);
            }
            else {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
            }
            /*this.store.get('username').then((val) => {
              });*/
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.menuItemHandler = function () {
        this.showSubmenu = !this.showSubmenu;
    };
    MyApp.prototype.openMen = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_pantalonhombre_pantalonhombre__["a" /* PantalonhombrePage */]);
    };
    MyApp.prototype.openWomen = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_pantalonmujer_pantalonmujer__["a" /* PantalonmujerPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header >\n    <ion-toolbar color="damagy">\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list id="sidenav">\n      <button align="center" menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        <ion-icon name={{p.icon}} item-start></ion-icon> \n        {{p.title}}\n      </button>\n\n\n      <ion-item (click)="menuItemHandler()"><ion-icon name="ios-contacts" item-start></ion-icon>Categoria  <ion-icon name="ios-arrow-down" item-end></ion-icon></ion-item>\n  <ion-item menuClose (click)="openMen()" submenu-item *ngIf="showSubmenu"><ion-icon name="ios-body" item-start></ion-icon>Hombres</ion-item>\n  <ion-item menuClose (click)="openWomen()" submenu-item *ngIf="showSubmenu"><ion-icon name="ios-woman" item-start></ion-icon>Mujeres</ion-item>\n  \n    </ion-list>\n  </ion-content>\n</ion-menu>\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CheckoutPage = /** @class */ (function () {
    //myDate2: String = new Date().toISOString().split('T')[0];
    function CheckoutPage(fb, platform, navCtrl, http, cart_data, alertCtrl, loadingCtrl) {
        var _this = this;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.http = http;
        this.cart_data = cart_data;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.items = '';
        this.total = 0;
        this.username = "";
        this.direccion = "";
        this.email = "";
        this.ubicacion = "";
        this.myForm = this.fb.group({
            username: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            phone: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(8), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(8)]],
            entrega: [''],
            month: [''],
            ci: [''],
            direccion: [''],
            email: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].email]],
        });
        this.cart_data.loadAllItemsOrdered().then(function (result) {
            _this.orders = result;
        });
        this.cart_data.getTotalBill().then(function (result) {
            _this.totalBill = result;
            console.log(_this.totalBill);
        });
        this.data = {};
        this.data.trans_info = '';
        this.data.username = '';
        this.data.phone = '';
        this.data.payment = '';
        this.data.response = '';
        this.http = http;
        this.name = localStorage.getItem('name');
    }
    CheckoutPage.prototype.sendSaleCart = function () {
        var _this = this;
        console.log(JSON.stringify(this.myForm.value));
        console.log(this.myForm.value.email);
        this.cart_data.sendSale(this.myForm.value.username, this.myForm.value.phone, this.myForm.value.direccion, this.myForm.value.email, this.myForm.value.ci, this.myForm.value.month).then(function (res) {
            _this.showMessage();
        }, function (err) {
            _this.showErrorMessage();
        });
    };
    CheckoutPage.prototype.showMessage = function () {
        var alert = this.alertCtrl.create({
            title: '<br><center>Pedido Exitoso</center></br>',
            subTitle: '<br><center>Su pedido se realizo exitosamente</center></br>.',
            buttons: ['OK']
        });
        alert.present();
        this.confirm();
        //this.navCtrl.push(HomePage);
        //this.navCtrl.pop();
    };
    CheckoutPage.prototype.showErrorMessage = function () {
        var alert = this.alertCtrl.create({
            title: '<br><center>Ops!</center></br>',
            subTitle: '<br><center>No se pudo realizar el pedido intente nuevamente.</br></center>',
            buttons: ['OK']
        });
        alert.present();
    };
    CheckoutPage.prototype.confirm = function () {
        window.location.reload();
    };
    /*let headers = new Headers();
headers.append('Content-Type', 'application/json' );

for(let order of this.orders) {
       this.items += order.title+",";
  }

let postParams = { trans_info: this.data.username + "-" + this.data.phone , trans_info2: this.items }

var link = 'http://localhost:8100/mystore/api.php';

this.http.post(link, JSON.stringify(postParams), {headers: headers})
.subscribe(data => {
    this.data.response = data["_body"];
    alert("Your orders has been received and will be processed shortly.");
}, error => {
    console.log("Error while processing your orders.");
});*/
    CheckoutPage.prototype.showConfirm = function (ndx) {
        var _this = this;
        console.log(ndx);
        var confirm = this.alertCtrl.create({
            title: 'Quitar este item del Carrito?',
            cssClass: 'alertcss',
            message: 'Confirmar Por Favor.',
            buttons: [
                {
                    text: 'Yes',
                    handler: function () {
                        _this.cart_data.removeItem(ndx);
                        _this.cart_data.getTotalBill().then(function (result) {
                            _this.totalBill = result;
                            console.log(_this.totalBill);
                        });
                    }
                },
                {
                    text: 'Cancel',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    CheckoutPage.prototype.sendConfirm = function (ndx) {
        var confirm = this.alertCtrl.create({
            title: 'Proceder con el pedido?',
            message: 'Por favor confirmar.',
            buttons: [
                {
                    text: 'Si',
                    handler: function () {
                        window.location.reload();
                        //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    CheckoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-checkout',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\checkout\checkout.html"*/'<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Carrito</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content      class="checkout" padding>\n\n  <h4>Productos:</h4>\n\n  <ion-list>\n    <ion-item-sliding *ngFor="let order of orders;let i = index;">\n      <ion-item>\n          <div class="order-list"><b>{{order.title}} || </b> <span><b>Cantidad:</b> {{order.cantidad}} ||</span><!-- <span><b>Precio Unidad:{{order.amount}} || </b></span> --><button style="    font-size: 0.99em;" round><b> Total Bs.</b>{{order.total}}</button> </div>\n      </ion-item>    \n      <ion-item-options>\n        <button ion-button color="danger" (click)="showConfirm(i);"><ion-icon name="trash"></ion-icon></button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n\n  <div>\n    <p style="font-size:18px;">Total: <strong>Bs.{{totalBill|number:0}}</strong></p>\n  </div>\n\n  <hr>\n  {{data.response}} \n  \n<div>\n  <form [formGroup]="myForm" (ngSubmit)="sendSaleCart()" novalidate>\n      <ion-list>\n        <ion-item>\n          <h2 align="center">Información del Cliente:</h2>\n        </ion-item>\n    \n    <ion-item>\n        <ion-input formControlName="username"  value="{{name}}" type="text" name="username" placeholder="Ingrese su nombre"></ion-input>\n    </ion-item>\n        <ion-item *ngIf="myForm.get(\'username\').errors && myForm.get(\'username\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'username\').hasError(\'required\')">* Campo requerido</p>\n    </ion-item>\n     <ion-item>\n      <ion-input formControlName="phone" type="tel" placeholder="Ingrese su número de contacto" ></ion-input>\n    </ion-item>\n<ion-item *ngIf="myForm.get(\'phone\').errors && myForm.get(\'phone\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'phone\').hasError(\'minlength\')">Min of 8 digitos</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'phone\').hasError(\'maxlength\')">Max of 8 digitos</p>\n    </ion-item>\n   <ion-item>\n      <ion-input formControlName="direccion" type="text" name="direccion" placeholder="Ingrese su Direccion"></ion-input>\n    </ion-item>\n\n     <ion-item>\n      <ion-input formControlName="email" type="email" name="email" placeholder="email"></ion-input>\n    </ion-item> \n     <ion-item *ngIf="myForm.get(\'email\').errors && myForm.get(\'email\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'email\').hasError(\'required\')">* Campo requerido</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'email\').hasError(\'email\')">Email no valido</p>\n    </ion-item>\n <ion-item>\n    <ion-label>Tipo de entrega</ion-label>\n    <ion-select formControlName="entrega">\n      <ion-option value="tienda">En Tienda</ion-option>\n      <ion-option value="ubicacion">En Ubicacion</ion-option>\n     </ion-select>\n    </ion-item>\n      <ion-item>\n        <ion-input  formControlName="ci" type="number"  name="ci" placeholder="Ingrese su número de Ci"></ion-input>\n      </ion-item>\n     <ion-item>\n        <ion-label>Fecha de Entrega</ion-label>\n      <ion-datetime  formControlName="month" displayFormat="YYYY MMM DD" ></ion-datetime>\n    </ion-item>\n        </ion-list>\n      <div padding>\n\n        <button ion-button block   [disabled]="myForm.invalid">Enviar mi pedido</button></div>\n  </form>\n</div>\n</ion-content>'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\checkout\checkout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__providers_cart_data__["a" /* CartData */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* LoadingController */]])
    ], CheckoutPage);
    return CheckoutPage;
}());

//# sourceMappingURL=checkout.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pantalonhombre_pantalonhombre__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pantalonmujer_pantalonmujer__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_checkout_checkout__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomePage = /** @class */ (function () {
    function HomePage(store, navCtrl, cart_data) {
        var _this = this;
        this.store = store;
        this.navCtrl = navCtrl;
        this.cart_data = cart_data;
        this.cart_data.getSize().then(function (result) {
            _this.total_ordered_items = result;
        });
    }
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage.prototype.gotoPastaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pantalonhombre_pantalonhombre__["a" /* PantalonhombrePage */]);
    };
    HomePage.prototype.gotoPizzaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pantalonmujer_pantalonmujer__["a" /* PantalonmujerPage */]);
    };
    HomePage.prototype.gotoStartersPage = function () {
    };
    HomePage.prototype.toCar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_checkout_checkout__["a" /* CheckoutPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title align="center" >Damagy Jeans</ion-title>\n    <div *ngIf="total_ordered_items?.length > 0" style="position:absolute;right:32px;top:18px;background-color:#488AFF;padding:4px 8px;border-radius:50%;color:#fff;font-size:10px;">\n        {{total_ordered_items.length}}\n    </div>\n    <ion-buttons end>\n        <button (click)="toCar()" style="background-color:transparent;"><ion-icon id="car" name="cart"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content  >\n\n  <h3 style="text-align: center;">Selecciona</h3>\n\n  <ion-card>\n    <img src="img/pantalones-ajustados-hombres.jpg"/>\n    <div class="card-title" (click)="gotoPastaPage()">Pantalones de Hombres</div>\n  </ion-card>\n\n  <ion-card>\n    <img src="img/pantalones-ajustados-hombres.jpg"/>\n    <div class="card-title" (click)=\'gotoPizzaPage()\'>Pantalones de Mujeres</div>\n  </ion-card>\n\n\n\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_cart_data__["a" /* CartData */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_register_register__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(store, alertCtrl, http, cart_data, fb, navCtrl, navParams, formBuilder) {
        this.store = store;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.cart_data = cart_data;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.myForm = this.fb.group({
            username: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            password: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
        });
        //this.store.set('username', JSON.stringify(this.myForm.value.username));
        //this.store.set('password', JSON.stringify(this.myForm.value.password));
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        this.nameUser = this.myForm.value.username;
        localStorage.setItem('username', this.myForm.value.username);
        console.log('this.myForm.value.username');
        console.log(this.store.set('username', JSON.stringify(this.myForm.value.username)));
        this.cart_data.loginUser(this.myForm.value.username, this.myForm.value.password).then(function (res) {
            _this.showMessage();
        }, function (err) {
            _this.showErrorMessage();
        });
    };
    LoginPage.prototype.showMessage = function () {
        var alert = this.alertCtrl.create({
            title: '<br><center>Succes</center></br>',
            subTitle: "<br><center>Bienvenido </br>" + this.nameUser + "<br> a Damagis Jeans</center></br>.",
            buttons: ['OK']
        });
        alert.present();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.showErrorMessage = function () {
        var alert = this.alertCtrl.create({
            title: '<br><center>Ops!</center></br>',
            subTitle: '<br><center>El usuario no existe, inténtelo otra vez</br></center>',
            buttons: ['OK']
        });
        alert.present();
    };
    LoginPage.prototype.signUp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\login\login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<style type="text/css">\n::-webkit-input-placeholder {\n  color: white !important; \n}\n:-moz-placeholder {\n  color: white !important; \n}\n:-ms-input-placeholder {\n  /* IE10+ */\n  color: white !important; \n}\n</style>\n\n<ion-content padding class="login" style="background-image:url(\'img/fondo.jpg\');background-repeat: no-repeat;background-size: 100% 100%">\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4></ion-col>\n      <ion-col col-4 style="margin-left: auto;margin-right: auto;display: block;">\n        <img src="img/icon.png" style="width:100%;height:auto;border-radius: 100%">\n      </ion-col>\n      <ion-col col-4></ion-col>\n    </ion-row>\n  </ion-grid>\n<form [formGroup]="myForm"  novalidate>\n  <ion-list class="list-form" style="margin-top:30px;margin-bottom: 30px">\n  	\n  	<ion-item style="padding:0px !important; border-bottom:none !important;border-top:none;font-size: 14px;margin-top:10px;background:none;border-bottom:1px solid white;box-shadow:none;">\n	    <ion-input style="color:white;border-bottom-color:white;box-shadow:none;" formControlName="username" type="text" placeholder="username"></ion-input>\n  	</ion-item>\n    <ion-item *ngIf="myForm.get(\'username\').errors && myForm.get(\'username\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'username\').hasError(\'required\')">Field is required</p></ion-item>\n  	<ion-item style="padding:0px !important; font-size: 14px;margin-top:10px;background:none;border-bottom:1px solid white;box-shadow:none;">\n	    <ion-input style="color:white;border-bottom-color:white;box-shadow:none;" formControlName="password" type="password" placeholder="password"></ion-input>\n  	</ion-item>\n <ion-item *ngIf="myForm.get(\'password\').errors && myForm.get(\'password\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'password\').hasError(\'required\')">Field is required</p>\n    </ion-item>\n  </ion-list>\n\n  <button style="margin-top: 15px;height: 35px;font-size: 14px;background: #10ABF4;" ion-button  block  (click)="login()">Login</button>\n\n\n  <button  style="margin-top: 15px;height: 35px;font-size: 14px;background: #10ABF4;"   ion-button block  (click)="signUp()">Register</button>\n</form>\n\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__providers_cart_data__["a" /* CartData */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PantalonhombrePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_checkout_checkout__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PantalonhombrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PantalonhombrePage = /** @class */ (function () {
    function PantalonhombrePage(fb, navCtrl, http, alertCtrl, cart_data) {
        var _this = this;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.cart_data = cart_data;
        this.cantidad = '';
        this.data;
        this.cart_data.loadAll().then(function (result) {
            _this.orders = result;
        });
        this.cart_data.getSize().then(function (result) {
            _this.total_ordered_items = result;
        });
        this.myForm = this.fb.group({
            cantidad: ['', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(1),
                    __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(2)]
            ],
        });
        this.cantidad = this.myForm.value.cantidad;
    }
    PantalonhombrePage.prototype.ionViewDidLoad = function () {
        console.log("entro aqui primero");
        this.getData();
    };
    PantalonhombrePage.prototype.getData = function () {
        var _this = this;
        this.http.get('assets/data/pantaloneshombre.json').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.data = data;
            console.log(_this.data);
        }, function (rej) { console.error("No hay respuesta", rej); });
    };
    PantalonhombrePage.prototype.showConfirm = function (item, price, cantidad, total) {
        var _this = this;
        console.log(cantidad);
        this.totalPrice = price * this.myForm.value.cantidad;
        console.log(this.totalPrice);
        var confirm = this.alertCtrl.create({
            title: 'Añadir al carrito?',
            message: 'Por favor confirme si quiere agregar el producto carrito',
            buttons: [
                {
                    text: 'Si',
                    handler: function () {
                        _this.cart_data.addItem(item, price, _this.myForm.value.cantidad, _this.totalPrice);
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    PantalonhombrePage.prototype.toCar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_checkout_checkout__["a" /* CheckoutPage */]);
    };
    PantalonhombrePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pantalonhombre',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\pantalonhombre\pantalonhombre.html"*/'<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Pantalones de Hombre</ion-title>\n    <div *ngIf="total_ordered_items?.length > 0" style="position:absolute;right:32px;top:18px;background-color:#488AFF;padding:4px 8px;border-radius:50%;color:#fff;font-size:10px;">\n        {{total_ordered_items.length}}\n    </div>\n    <ion-buttons end>\n        <button (click)="toCar()" style="background-color:transparent;"><ion-icon id="car" name="cart"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n<form [formGroup]="myForm" novalidate>\n<ion-card *ngFor="let dat of data" style="background-color:#f6f6f6;margin-bottom:25px;">\n  <img src="img/{{ dat.img }}.jpg" >\n  <ion-card-content>\n    <ion-card-title>\n      {{ dat.title }}\n    </ion-card-title>\n    <div><h4>Color</h4>\n<ion-item>\n  <ion-select>\n <ion-option [value]="item.value" *ngFor="let item of dat.color;let i = index;" style="margin-bottom:30px;">{{item.value}}</ion-option>\n      </ion-select>  \n</ion-item></div>\n\n   <div><h4>Talla</h4>\n<ion-item>\n  <ion-select >\n <ion-option [value]="item.name" *ngFor="let item of dat.tallas;let i = index;" style="margin-bottom:30px;">{{item.value}}</ion-option>  </ion-select>\n</ion-item></div>\n\n<div>\n  <h4>Cantidad</h4>\n <ion-item  >\n      \n    <ion-input  formControlName="cantidad" placeholder="Ingrese cantidad" type="tel"    ></ion-input>\n    <!--<p color="danger" ion-text *ngIf="maxlength="20" && minLength="1".hasError(\'cantidad\')">It is not an cantidad 30</p>-->\n\n  </ion-item>\n  <ion-item *ngIf="myForm.get(\'cantidad\').errors && myForm.get(\'cantidad\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'cantidad\').hasError(\'required\')">Campo Requedido</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'cantidad\').hasError(\'maxlength\')"> Cantidad max 99</p>\n    </ion-item>\n</div>\n    <p *ngFor="let item of dat.items;let i = index;" style="margin-bottom:30px;">\n    	{{ item.details }}<br><br>\n      \n      <span style="font-size:15px;font-weight:bold;float:right;">Bs{{ item.price }}</span>\n      <button  ion-button  (click)="showConfirm(dat.title,item.price,cantidad)"  [disabled]="myForm.invalid"><ion-icon name="cart"></ion-icon>Agregar al Carrito</button>\n    </p>\n    \n  </ion-card-content>\n</ion-card>\n\n</form>\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\pantalonhombre\pantalonhombre.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */]) === "function" && _e || Object])
    ], PantalonhombrePage);
    return PantalonhombrePage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=pantalonhombre.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PantalonmujerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_checkout_checkout__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PantalonmujerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PantalonmujerPage = /** @class */ (function () {
    function PantalonmujerPage(fb, navCtrl, http, alertCtrl, cart_data) {
        var _this = this;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.cart_data = cart_data;
        this.cantidad = '';
        this.data;
        this.getData();
        this.cart_data.loadAll().then(function (result) {
            _this.orders = result;
            console.log(_this.orders);
        });
        this.cart_data.getSize().then(function (result) {
            _this.total_ordered_items = result;
            console.log(_this.total_ordered_items);
        });
        this.myForm = this.fb.group({
            cantidad: ['', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required,
                    __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].minLength(1),
                    __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].maxLength(2)]
            ],
        });
        this.cantidad = this.myForm.value.cantidad;
    }
    PantalonmujerPage.prototype.getData = function () {
        var _this = this;
        this.http.get('assets/data/pantalonesmujer.json').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.data = data;
            console.log(_this.data);
        }, function (rej) { console.error("Could not load local data", rej); });
    };
    PantalonmujerPage.prototype.showConfirm = function (item, price, cantidad, total) {
        var _this = this;
        console.log(cantidad);
        this.totalPrice = price * this.myForm.value.cantidad;
        console.log(this.totalPrice);
        var confirm = this.alertCtrl.create({
            title: 'Añadir al carrito?',
            message: 'Por favor confirme si quiere agregar el producto carrito',
            buttons: [
                {
                    text: 'Si',
                    handler: function () {
                        _this.cart_data.addItem(item, price, _this.myForm.value.cantidad, _this.totalPrice);
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    PantalonmujerPage.prototype.toCar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_checkout_checkout__["a" /* CheckoutPage */]);
    };
    PantalonmujerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pantalonmujer',template:/*ion-inline-start:"D:\Juan Pablo\test\shoppingstore\src\pages\pantalonmujer\pantalonmujer.html"*/'<ion-header>\n  <ion-navbar color="damagy">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Pantalones de Mujer</ion-title>\n    <div *ngIf="total_ordered_items?.length > 0" style="position:absolute;right:32px;top:18px;background-color:#488AFF;padding:4px 8px;border-radius:50%;color:#fff;font-size:10px;">\n        {{total_ordered_items.length}}\n    </div>\n    <ion-buttons end>\n        <button  (click)="toCar()" style="background-color:transparent;"><ion-icon id="car" name="cart"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n<form [formGroup]="myForm" novalidate>\n\n<ion-card *ngFor="let dat of data" style="background-color:#f6f6f6;margin-bottom:25px;">\n  <img src="img/{{ dat.img }}.jpeg" />\n  <ion-card-content>\n    <ion-card-title>\n      {{ dat.title }}\n    </ion-card-title>\n    <div><h4>Color</h4>\n<ion-item>\n  <ion-select>\n <ion-option [value]="item.value" *ngFor="let item of dat.color;let i = index;" style="margin-bottom:30px;">{{item.value}}</ion-option>\n      </ion-select>  \n</ion-item></div>\n\n   <div><h4>Talla</h4>\n<ion-item>\n  <ion-select >\n <ion-option [value]="item.name" *ngFor="let item of dat.tallas;let i = index;" style="margin-bottom:30px;">{{item.value}}</ion-option>  </ion-select>\n</ion-item></div>\n<div>\n  <h4>Cantidad</h4>\n <ion-item  >\n      \n    <ion-input  formControlName="cantidad" placeholder="Ingrese cantidad" type="tel"    ></ion-input>\n    <!--<p color="danger" ion-text *ngIf="maxlength="20" && minLength="1".hasError(\'cantidad\')">It is not an cantidad 30</p>-->\n\n  </ion-item>\n  <ion-item *ngIf="myForm.get(\'cantidad\').errors && myForm.get(\'cantidad\').dirty">\n      <p color="danger" ion-text *ngIf="myForm.get(\'cantidad\').hasError(\'required\')">Campo Requedido</p>\n      <p color="danger" ion-text *ngIf="myForm.get(\'cantidad\').hasError(\'maxlength\')"> Cantidad max 99</p>\n    </ion-item>\n</div>\n    <p *ngFor="let item of dat.items;let i = index;" style="margin-bottom:30px;">\n      {{ item.details }}<br><br>\n      \n      <span style="font-size:15px;font-weight:bold;float:right;">Bs{{ item.price }}</span>\n      <button *ngIf="i==0" ion-button block (click)="showConfirm(dat.title,item.price,cantidad);" [disabled]="myForm.invalid"><ion-icon name="cart"></ion-icon>Agregar a Carrito</button>\n    </p>\n  </ion-card-content>\n</ion-card>\n</form>\n\n</ion-content>\n'/*ion-inline-end:"D:\Juan Pablo\test\shoppingstore\src\pages\pantalonmujer\pantalonmujer.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_cart_data__["a" /* CartData */]) === "function" && _e || Object])
    ], PantalonmujerPage);
    return PantalonmujerPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=pantalonmujer.js.map

/***/ })

},[206]);
//# sourceMappingURL=main.js.map